<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

// About Object Oriented Programming
// 關於物件導向
// 設定新的class（類別）
class Person {

    public $name;   # 公開屬性
    public $mobile; # 公開屬性
    private $sno = 'A1234';   # 私有屬性

    // 建構函式（建構式）：當物件生成的同時，強制預先去實作執行物件本身的功能。
    // 建構式的2個基本原則：
    // 1. 建構式僅用來做為設置"預設屬性值"的存在
    // 2. 建構式僅用來做為預載函式功能執行的存在
    // REF: https://ithelp.ithome.com.tw/articles/10114761
    // 物件在使用new建立之後，會自動被呼叫的函式
    // 'John' and '0910' are default values
    function __construct($name='John', $mobile='0910')
    {
        // 將class裡面的變數傳到函式的參數中
        $this->name = $name;
        $this->mobile = $mobile;
        echo '__construct<br>';
    }

    // getter function
    // 設定此函式表示開啟private屬性變數的讀取(read)權限
    // 取得private屬性變數裏面的值
    function getSno(){
        return $this->sno;
    }

    // setter function
    // 設定此函式表示開啟private屬性變數的寫入(write)權限
    // 設定private屬性變數新的值
    function setSno($sno){
        $this->sno = $sno;
    }

    // 在名為Person的class裡面設定getter及setter函式，class以外則看不到，稱為"資料的封裝"
}

// 建立新物件'p'
$p = new Person('Flora', '0918');

// 指定物件屬性'name'
$p->name = 'Peter';

// 取得private屬性變數的值：'A1234'
echo $p->getSno();
echo '<br>';

// 設定private屬性變數新的值
$p->setSno('B567');
echo $p->getSno();
echo '<br>';

echo "$p->name <br>"; // might be 'Flora'

?>

</body>
</html>