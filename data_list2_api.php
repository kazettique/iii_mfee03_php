<?php
    // 摘要：此檔案是提供資料，無畫面呈現
    // 顯示頁碼

    // 連線資料庫
    require __DIR__ . '/__connect_db.php';

    // 每頁有幾筆資料
    $per_page = 30;

    // 設定初始值
    $result = [
        'success' => 0,
        'page' => 0,
        'totalRow' => 0,        // 總資料筆數
        'perPage' => $per_page,
        'totalPages' => 0,      // 總頁數
        'data' => [],           // 資料陣列
        'errorCode' => 0,
        'errorMsg' => '',
    ];

    // 注意！GET必須全大寫！
    $page = isset($_GET['page']) ? intval($_GET['page']) : 1;

    // 計算總筆數
    $t_sql = "SELECT COUNT(1) FROM address_book";
    // PDO::query — Executes an SQL statement, returning a result set as a PDOStatement obj
    // REFERENCE: http://php.net/manual/en/pdo.query.php
    $t_stmt = $pdo->query($t_sql);
    $total_rows = $t_stmt->fetch(PDO::FETCH_NUM)[0];
    // intval: 將$total_rows由字串轉換成數字
    $result['totalRow'] = intval($total_rows);

    // 計算總頁數
    $total_pages = ceil($total_rows / $per_page);
    $result['totalPages'] = $total_pages;


    if($page < 1) $page = 1;
    if($page > $total_pages) $page = $total_pages;
    // Select a sheet from database
    // "ORDER BY sid DESC": 以sid排序，順序為降冪
    // LIMIT: 抓取範圍。從第1個（Index "0"）開始，拿10個。
    // REFERENCE: https://www.geeksforgeeks.org/php-mysql-limit-clause/
    // ($page - 1) * $per_page: 抓取目前頁數
    // $per_page: 抓取總頁數
    $sql = sprintf("SELECT * FROM address_book ORDER BY sid ASC LIMIT %s, %s", ($page - 1) * $per_page, $per_page);
    $stmt = $pdo->query($sql);

    // 所有資料一次拿出來
    $result["data"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $result['success'] = true;

    // json_encode: 將陣列轉換成json字串
    // JSON_UNESCAPED_UNICODE: unicode的字元不做跳脫
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
