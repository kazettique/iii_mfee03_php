<?php
// 摘要：使用API新增資料
// 重點：

require __DIR__. '/__connect_db.php';

// 頁面名稱
$page_name = 'data_insert';

// 檔頭：告訴瀏覽器要輸出的格式為JSON
header('Content-type: application/json');

// 設定用戶輸入資料後回傳的陣列資訊之初始值
$result = [
    'success' => false,
    'errorCode' => 0,
    'errorMsg' => '資料輸入不完整',
    'post' => [], // 做 echo 檢查
];

if(isset($_POST['checkme'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    $result['post'] = $_POST; // 做 echo(回應) 檢查

    // 若姓名、電子郵件、手機號碼欄位任一欄為空值，回傳'400'錯誤代碼
    if (empty($name) or empty($email) or empty($mobile)) {
        $result['errorCode'] = 400;
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    // 檢查 email 格式
    // 若輸入之Email格式錯誤，回傳405錯誤代碼
    if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $result['errorCode'] = 405;
        $result['errorMsg'] = 'Email 格式不正確';
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    // 檢查 name 長度
    // 若輸入之Email格式錯誤，回傳406錯誤代碼
//    if (mb_check_encoding($name, utf8)) {
//        $result['errorCode'] = 406;
//        $result['errorMsg'] = '姓名過短';
//        echo json_encode($result, JSON_UNESCAPED_UNICODE);
//        exit;
//    }

    // to do: 檢查 mobile 長度
    // to do: comment of hightlight in phpsorm
    // REF: https://www.jetbrains.com/help/phpstorm/using-todo.html

    $sql = "INSERT INTO `address_book`(
            `name`, `email`, `mobile`, `birthday`, `address`
            ) VALUES (
              ?, ?, ?, ?, ?
            )";

    try {
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            $_POST['name'],
            $_POST['email'],
            $_POST['mobile'],
            $_POST['birthday'],
            $_POST['address'],
        ]);

        if ($stmt->rowCount()==1) {
            $result['success'] = true;
            $result['errorCode'] = 200;
            $result['errorMsg'] = '';
        } else {
            $result['errorCode'] = 402;
            $result['errorMsg'] = '資料新增錯誤';
        }
    } catch (PDOException $ex) {
            $result['errorCode'] = 403;
            $result['errorMsg'] = 'Email 重複輸入';
    }
}

// 將$result轉換成JSON字串並回傳
echo json_encode($result, JSON_UNESCAPED_UNICODE);


