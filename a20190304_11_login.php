<?php
    // 摘要：製作登入頁面

    session_start();

    // 初始化兩個空字串，將在表單裡面使用
    $user = '';
    $password = '';

    // 若使用者帳號密碼有設定值
    if(isset($_POST['user']) && isset($_POST['password']))
        // 將使用者所輸入的帳號及密碼賦予給兩個變數
        $user = $_POST['user'];
        $password = $_POST['password'];

        // 若名稱和密碼正確，將名稱登入$_SESSION
        if($user === 'han' && $password === '1234') {
            // 'user'為$_SESSION裡面的一個(陣列)元素
            $_SESSION['user'] = 'han';
        } else {
            $msg = '賬號或密碼錯誤';
        }
    }

?>
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.css">
    <script src="./js/jquery-3.3.1.js"></script>
    <script src="./bootstrap/js/bootstrap.bundle.js"></script>
    <style>
        .container {
            margin: 3rem auto auto auto;
            max-width: 50%;
        }
    </style>
</head>
<body>


<div class="container">
    <!-- 如果沒有登入，才顯示登入表單 -->
    <?php if(! isset($_SESSION['user'])): ?>

        <?php if(isset($msg)): ?>
            <div class="alert alert-danger" role="alert">
                <?= $msg ?>
            </div>
        <?php endif ?>

    <form method="post">
        <div class="form-group">
            <!-- 在'value'設定顯示使用者輸入的帳號名稱 -->
            <input type="text" class="form-control" name="user" placeholder="用戶名稱" value="<?= $user ?>">
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <!-- 在'value'設定顯示使用者輸入的密碼 -->
            <input type="password" class="form-control" name="password" placeholder="密碼" value="<?= $password ?>">
        </div>

        <button type="submit" class="btn btn-primary">登入</button>
    </form>

    <!-- 如果有登入，則顯示歡迎訊息 -->
    <?php else: ?>

        <div class="alert alert-warning" role="alert">
            <?= $_SESSION['user'] ?>，您好！
        </div>
        <br>
        <!-- 連結到登出頁面的php程式 -->
        <a href="./a20190304_12_logout.php" class="btn btn-danger">登出</a>

    <?php endif; ?>


</body>
</html>