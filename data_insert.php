<?php
// 摘要：新增自來喔
// 重點：前端的表單檢查

//require __DIR__. '/__cred.php';
require __DIR__. '/__connect_db.php';
$page_name = 'data_insert';

// 先賦予空值
$name = '';
$email = '';
$mobile = '';
$birthday = '';
$address = '';

// htmlentities: 防止<scirpt>程式的入侵
// 在表單輸入值格式有錯誤時，保留用戶輸入的值
if(isset($_POST['checkme'])) {
    $name = htmlentities($_POST['name']);
    $email = htmlentities($_POST['email']);
    $mobile = htmlentities($_POST['mobile']);
    $birthday = htmlentities($_POST['birthday']);
    $address = htmlentities($_POST['address']);

    // 第一種方法
    /*
    if (isset($_POST['checkme'])) {
        // sprintf: 將值填入
        // "INSERT INTO ...": mysql的資料插入的語法
        $sql = sprintf("INSERT INTO `address_book`(
            `name`, `email`, `mobile`, `birthday`, `address`
            ) VALUES (
              '%S', '%S', '%S', '%S', '%S'
            )",
        // 將各欄位插入的資料指定至$_POST陣列
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['birthday'],
        $_POST['address']
        );
    $stmt = $pdo->query($sql);
    }
    */

    // 第二種方法
    // 為了防範SQL injection
    /*
    $sql = sprintf("INSERT INTO `address_book` (
          `name`, `email`, `mobile`, `birthday`, `address`
          ) VALUES (
          // 因為PDO::quote會自動加上單引號，因此要將%s單引號拿掉（有別於第一種方法）
          %s, %s, %s, %s, %s
          )",
        // quote: Escape Single Quote In String In SQL Server
        // 使用pdo的quote函式將單引號跳脫
        $pdo->quote($_POST['name']),
        $pdo->quote($_POST['email']),
        $pdo->quote($_POST['mobile']),
        $pdo->quote($_POST['birthday']),
        $pdo->quote($_POST['address'])
    );
    // 測試SQL長什麽樣子
    // echo $sql; exit;
    // die; // 同exit的功能
    $stmt = $pdo->query($sql);
    */

    // 第三種方法
    // sql的模板
    $sql = "INSERT INTO `address_book`(
            `name`, `email`, `mobile`, `birthday`, `address`
            ) VALUES (
              ?, ?, ?, ?, ?
            )";

    try {
        // 'PDO::prepare': Prepares a statement for execution and returns a statement object
        // 因為前面還沒賦值(標示為"?")，所以不能使用query，在此使用prepare
        // prepare之後得到一個PDOStatement的物件
        // prepare是並沒有執行，
        $stmt = $pdo->prepare($sql);

        // execute: Executes a prepared statement
        // 執行$stmt
        $stmt->execute([
            $_POST['name'],
            $_POST['email'],
            $_POST['mobile'],
            $_POST['birthday'],
            $_POST['address'],
        ]);

        // 設定bootstrap的alert樣式及文字訊息
        // rowCount()==1: 資料新增成功
        if ($stmt->rowCount()==1) {
            $success = true;
            // 提示：php關聯式陣列
            $msg = [
                'type' => 'success',
                'info' => '資料新增成功',
            ];
        } else {
            $msg = [
                'type' => 'danger',
                'info' => '資料新增錯誤',
            ];
        }
    } catch (PDOException $ex) {

        $msg = [
            'type' => 'danger',
            'info' => 'Email 重複輸入',
        ];
    }
}
?>

<?php include __DIR__. '/__html_head.php';  ?>
<?php include __DIR__. '/__navbar.php';  ?>
    <style>
        .form-group small {
            color: red !important;
        }
    </style>

<div class="container">

    <div class="row">
        <div class="col-lg-6">
            <!-- 如果有錯誤或成功訊息則顯示訊息框 -->
            <?php if (isset($msg)): ?>
                <div class="alert alert-<?= $msg['type'] ?>" role="alert">
                    <?= $msg['info'] ?>
                </div>
            <?php endif ?>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">新增資料
                        <!-- 測試是否有資料新增至新欄位 -->
                        <!--
                        <?php
                        if (isset($stmt)) {
                            echo $stmt->rowCount();
                        }
                        ?>
                        -->
                    </h5>

                    <!-- 呼叫checkForm涵式檢查所填的表單内容 -->
                    <form name="form1" method="post" onsubmit="return checkForm();">
                        <input type="hidden" name="checkme" value="check123">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder=""
                                   value="<?= $name ?>">
                            <small id="nameHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="email">電子郵件</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder=""
                                   value="<?= $email ?>">
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder=""
                                   value="<?= $mobile ?>">
                            <small id="mobileHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" placeholder="YYYY-MM-DD"
                                   value="<?= $birthday ?>">
                            <small id="birthdayHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" id="address" name="address" cols="30" rows="3"><?= $address ?></textarea>
                            <small id="addressHelp" class="form-text text-muted"></small>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script>
        // 必填的欄位名稱置入陣列
        const fields = [
            'name',
            'email',
            'mobile',
            'birthday',
            'address',
        ];

        // 拿到每個欄位的參照
        const fs = {};
        for (let v of fields) {
            // 抓取form1表單的內容，並置入fs陣列
            fs[v] = document.form1[v];
        }
        console.log(fs);
        console.log('typeof fs:', typeof(fs));
        // console.log(fs.name) 拿到的是html element的值,為"物件"
        console.log('fs.name:', fs.name);
        console.log('typeof fs.name:', typeof(fs.name));
        // console.log(fs.name.outerHTML) 拿到的是outerHTML裡面的值,為"字串"
        console.log('fs.name:', fs.name.outerHTML);
        console.log('typeof fs.name.outerHTML:', typeof(fs.name.outerHTML));

        // 函式：檢查用戶輸入表單的資訊格式是否正確
        const checkForm = () => {
            let isPassed = true;

            // 拿到每個欄位的值
            const fsv = {};

            for (let v of fields) {
                // 給'key'，拿到對應的'value'
                fsv[v] = fs[v].value;
            }
            console.log('fsv:', fsv);

            // let name = document.form1.name.value;
            // let email = document.form1.email.value;
            // let mobile = document.form1.mobile.value;

            let email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            let mobile_pattern = /^09\d{2}\-?\d{3}\-?\d{3}$/;

            // 使用for( ... of ... )
            // 'v'為每個索引的值
            for (let v of fields) {
                // 將輸入欄樣式初始化
                fs[v].style.borderColor = '#cccccc';
                // 將提示訊息初始化（移除）
                document.querySelector('#' + v + 'Help').innerHTML = '';

            }

            // 使用for( ... in ... )
            /*
            // 'k'為索引值
            for (let k in fields) {
                document.form1[fields[k]].style.borderColor = '#cccccc';
                document.querySelector('#' + fields[k] + 'Help').innerHTML = '';
            }
            */

            // 檢查姓名的長度
            if(fsv.name.length < 2) {
                fs.name.style.borderColor = 'red';
                // document.form1.name.style.borderColor = 'red';
                // 顯示提示訊息
                document.querySelector('#nameHelp').innerHTML = '請填寫正確的姓名!';
                isPassed = false;
            }

            // 檢查Email的格式
            if(! email_pattern.test(fsv.email)) {
                document.form1.email.style.borderColor = 'red';
                // 顯示提示訊息
                document.querySelector('#emailHelp').innerHTML = '請填寫正確的 Email!';
                isPassed = false;
            }

            // 檢查手機號碼的格式
            if(! mobile_pattern.test(fsv.mobile)){
                fs.mobile.style.borderColor = 'red';
                // 顯示提示訊息
                document.querySelector('#mobileHelp').innerHTML = '請填寫正確的手機號碼!';
                isPassed = false;
            }

            return isPassed;
        };

    </script>
</div>

<?php include __DIR__. '/__html_foot.php';  ?>