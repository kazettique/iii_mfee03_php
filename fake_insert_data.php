<?php
// 摘要：建立大量的假資料
// 學習重點：
// 1. PDO::beginTransaction
// 2. PDO::commit
// 3. microtime函式

require __DIR__. '/__connect_db.php';

// microtime: Return current Unix timestamp with microseconds
// 顯示開始時間（毫秒）
$begin = microtime(true);
echo 'Start time:'. $begin. '<br>';

$sql = "INSERT INTO `address_book`(
            `name`, `email`, `mobile`, `birthday`, `address`
            ) VALUES (
              ?, ?, ?, ?, ?
            )";

$stmt = $pdo->prepare($sql);

// 'PDO::beginTransaction': Initiates a transaction
// transaction(交易): 就是當有兩段要執行的SQL語法，希望要嘛就兩個SQL語句都執行成功，
// 如果其中一行出現錯誤就恢復原始資料，這時候就要使用transaction。
// REF: http://italwaysrainonme.blogspot.com/2012/11/mysql-myisam.html
// 開始 Transaction
$pdo-> beginTransaction();
// 在寫入資料前使用beginTransaction
// 若果有大量資料處理執行的時候才使用transaction
// 簡單的執行則不需要使用

for($i=1; $i<300; $i++) {
    $stmt->execute([
        "李小明$i",
        "ming{$i}@gmail.com",
        "0918$i",
        "1990-02-03",
        "台南市$i",
    ]);
}

// 'PDO::commit': Commits a transaction
// PS. 沒有提交的話，transaction中所做的處理結果會被放棄
// 不是所有的地方都需要用transaction
// 一堆sql要處理的時候，才需要用到transaction
// 提交 Transaction
$pdo->commit();

$end = microtime(true);
echo 'End time: '. $end. '<br>';
echo 'Time Consumed: '. ($end-$begin). '<br>';

// NOTE:
// 目前所使用的儲存引擎（位於phpmyadmin）為InnoDB，此儲存引擎才有transaction的功能
// 其他的儲存引擎包括：
// MEMORY：將資料儲存在記憶體當中，系統關機之後所儲存的資料就會消失
// CSV：即文字檔
// BLACKHOLE: 黑洞，進去的資料就出不來，供測試用