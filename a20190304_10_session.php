<?php
    // 摘要：使用session的讀寫

    // 使用session_start去啟動cookie
    // session_start — Start new or resume existing session
    session_start();

    // 用$_SESSION陣列來讀取session
    // 檢查是否有名為'my'的key
    // 如果沒有，設定一個'my'的key，並賦予值'1'
    if(! isset($_SESSION['my'])) {
        $_SESSION['my'] = 1;
    // 若有'my'的key，將其值+1
    } else {
        $_SESSION['my']++;
    }
?>
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

    echo $_SESSION['my'];

?>

</body>
</html>