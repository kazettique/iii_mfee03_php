<?php

// Insert ?a=2&b=3 on site directory
// echo $_GET['a'] + $_GET['b'];

// The intval() function is an inbuilt function in PHP which returns the integer value of a variable.
// In intval(), when the value is a string, return 0
$a = isset($_GET['a']) ? intval($_GET['a']) : 0;
$b = isset($_GET['b']) ? intval($_GET['b']) : 0;

echo $a + $b;