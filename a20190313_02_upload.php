<?php
// 摘要：上傳多個檔案

$upload_dir = __DIR__. '/uploads/';

if(empty($_FILES['my_file'])){
    exit;
}

// my_file上傳檔案的陣列，error為(個別)錯誤訊息的陣列
// $_FILES包含my_file及error兩個陣列
foreach($_FILES['my_file']['error'] as $k=>$error){
    // Value: 0; There is no error, the file uploaded with success.
    // REF: http://php.net/manual/en/features.file-upload.errors.php
    // 其值為0時，代表沒有錯誤
    if($error == UPLOAD_ERR_OK){
        move_uploaded_file(
            // 原始檔名及暫存檔名
            $_FILES['my_file']['tmp_name'][$k],
            // 上傳目標資料匣
            $upload_dir. $_FILES['my_file']['name'][$k]
        );

    }
}

echo 'OK';









