<?php
    // 摘要：第二種作法
    // 利用'#'連結來切換資料頁面，不會頁面刷新
    // 因為頁面不刷新，所以資料流量較小
    // AJAX的方式

    // Import the database
    require __DIR__. '/__connect_db.php';

    $page_name = 'data_list2';

?>

<?php include __DIR__. '/__html_head.php';  ?>
<?php include __DIR__. '/__navbar.php';  ?>

<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination pagination-sm">
                    <!-- 前端直接生成畫面，而非從後端 -->
                    <!-- 後端的人只需要提供API，不需要知道畫面長什麼樣子，就可以先寫功能 -->
                    <?php /*
                    <li class="page-item <?= $page<=1 ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page-1 ?>">&lt;</a>
                    </li>
                    <?php for($i=1; $i<=$total_pages; $i++): ?>
                        <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                            <a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a>
                        </li>
                    <?php endfor ?>
                    <li class="page-item <?= $page>=$total_pages ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page+1 ?>">&gt;</a>
                    </li>
                    */ ?>
                </ul>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Address</th>
                    <th scope="col">Birthday</th>
                </tr>
                </thead>
                <tbody id="data_body">
                <?php /*
                <?php foreach($rows as $row): ?>
                    <tr>
                        <td><?= $row['sid'] ?></td>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['email'] ?></td>
                        <td><?= $row['mobile'] ?></td>
                        <td><?= $row['address'] ?></td>
                        <td><?= $row['birthday'] ?></td>
                    </tr>
                <?php endforeach; ?>
                */ ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script>
    // page不一定要設定為1
    let page = 1;
    let ori_data;
    const ul_pagi = document.querySelector('.pagination');
    const data_body = document.querySelector('#data_body');

    // template，顯示的資料欄位之通用格式
    // 使用underscore套件之"<%= %>"標籤
    // "<%= %>"中的變數會抓取資料表的欄位名稱
    const tr_str = `<tr>
                        <th><%= sid %></th>
                        <th><%= name %></th>
                        <th><%= email %></th>
                        <th><%= mobile %></th>
                        <th><%= address %></th>
                        <th><%= birthday %></th>
                    </tr>`;

    // _.template是一個underscore.js08的函式
    // REF: https://underscorejs.org/#template

    // 資料列(row a.k.a <tr>)的樣板(template)
    const tr_func = _.template(tr_str);

    const pagi_str = `<li class="page-item <%= active %>">
                        <a class="page-link" href="#<%= page %>">
                            <%= page %>
                        </a>
                      </li>`;

    // 頁碼的樣板(template)
    const pagi_func = _.template(pagi_str);

    const myHashChange = () => {
        // Remove the "#" symbol by using 'slice'
        // REF: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
        // location.hash: JS(DOM?)內建功能
        // The hash property sets or returns the anchor part of a URL, including the hash sign (#).
        let h = location.hash.slice(1);
        page = parseInt(h);
        // The isNaN() function determines whether a value is NaN or not.
        // To prevent other input value i.e. #abc, it will return '1' in the IF statement below
        if (isNaN(page)) {
            page = 10;
        }
        // ul_pagi.innerHTML = page;

        fetch('data_list2_api.php?page=' + page)
            .then(res => {
                return res.json();
            })
            .then(json => {
                ori_data = json;
                console.log(ori_data);

                let str = '';

                // for ( ... in ... ): gets INDEX from thr array.
                // for(let s in ori_data.data){
                //     str += tr_func(ori_data.data[s]);
                // }

                // for ( ... of ... ): gets VALUE from the array.
                // ori_data.data: 塞資料
                for(let val of ori_data.data){
                    str += tr_func(val);
                }
                data_body.innerHTML = str;

                str = '';

                for (let i = 1; i <= ori_data.totalPages; i++) {
                    let active = ori_data.page === i ? 'active' : '';
                    str += pagi_func({
                        active: active,
                        page: i
                    });
                }

                // console.log(str); // for test use
                // ↑↑ Expected result:
                // <li class="page-item ">
                //     <a class="page-link" href="#1">
                //         1
                //     </a>
                // </li>
                // <li class="page-item ">
                //     <a class="page-link" href="#2">
                //         2
                //     </a>
                // </li>
                //    .
                //    .
                //    .
                // <li class="page-item ">
                //     <a class="page-link" href="#10">
                //         10
                //     </a>
                // </li>

                ul_pagi.innerHTML = str;

            });
    };

    // The "hashchange" event is fired when the fragment identifier of the URL has changed
    // PS. The part of the URL beginning with and following the # symbol.
    window.addEventListener('hashchange', myHashChange);
    myHashChange();

</script>

<?php include __DIR__ . '/__html_foot.php'; ?>