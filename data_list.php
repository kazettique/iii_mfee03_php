<?php
    // 帳號密碼的認證：登入後才能進入該頁面
//    require __DIR__. '/__cred.php';
    // Import the database
    require __DIR__. '/__connect_db.php';

    // 頁面名稱
    // 'page_name'變數用於'__navbar.php'裡navbar的class使用
    $page_name = 'data_list';

    // 一頁有5筆資料
    $per_page = 30;

    // CAUTION! The GET should be all capitalized!
    // 用GET方法去跳轉頁面
    // PS.也可以使用POST，但是比較麻煩
    $page = isset($_GET['page']) ? intval($_GET['page']) : 1;

    // COUNT(): 計算資料總筆數
    // COUNT(1): 每一筆新資料都用'1'取代，因為這裡只是要計數，沒有要讀取欄位的值
    // 也可以是使用COUNT(*)或COUNT(sid)
    $t_sql = "SELECT COUNT(1) FROM address_book";
    // PDO::query — Executes an SQL statement, returning a result set as a PectDOStatement obj
    // REFERENCE: http://php.net/manual/en/pdo.query.php
    $t_stmt = $pdo->query($t_sql);
    $total_rows = $t_stmt->fetch(PDO::FETCH_NUM)[0];

    // 算總頁數
    $total_pages = ceil($total_rows / $per_page);

    // 若輸入頁數小於1，則顯示1；大於最大頁數，則顯示最大頁數
    // 防止在網址輸入POST方法所造成的程式錯誤
    if($page < 1) $page = 1;
    if($page > $total_pages) $page = $total_pages;

    // Select a sheet from database
    // "ORDER BY sid DESC": 以sid排序，順序為降冪
    // LIMIT: 抓取範圍。從第1個（Index "0"）開始，拿10個。
    // REFERENCE: https://www.geeksforgeeks.org/php-mysql-limit-clause/
    // ($page - 1) * $per_page: 抓取目前頁數
    // $per_page: 抓取總頁數
    $sql = sprintf("SELECT * FROM address_book ORDER BY sid ASC LIMIT %s, %s", ($page - 1) * $per_page, $per_page);
    $stmt = $pdo->query($sql);

    // Grab all data from database
    //所有資料一次拿出來
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

<?php include __DIR__. '/__html_head.php';  ?>
<?php include __DIR__. '/__navbar.php';  ?>

<div class="container">
    <!-- 顯示目前頁數及總頁數 -->
    <div><?= $page. " / ". $total_pages ?></div>

    <div class="row">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination pagination-sm">
                    <li class="page-item <?= $page<=1 ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=1">&lt;&lt;</a>
                    </li>
                    <li class="page-item <?= $page<=1 ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page-1 ?>">&lt;</a>
                    </li>
                    <?php for($i=1; $i<=$total_pages; $i++): ?>
                        <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                            <a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a>
                        </li>
                    <?php endfor ?>
                    <li class="page-item <?= $page>=$total_pages ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page+1 ?>">&gt;</a>
                    </li>
                    <li class="page-item <?= $page>=$total_pages ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $total_pages ?>">&gt;&gt;</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col"><i class="fas fa-edit"></i></th>
                        <th scope="col">SID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Mobile</th>
                        <th scope="col">Address</th>
                        <th scope="col">Birthday</th>
                        <th scope="col"><i class="fas fa-trash-alt"></i></th>
                    </tr>
                </thead>
                <tbody>
                <!-- Assign all data to html table -->
                <?php foreach($rows as $row): ?>
                    <tr>
                        <td>
                            <a href="data_edit.php?sid<?= $row['sid'] ?>">
                                <i class="fas fa-edit"></i>
                            </a>
                        </td>
                        <td><?= $row['sid'] ?></td>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['email'] ?></td>
                        <td><?= $row['mobile'] ?></td>
                        <!-- strip_tags: Strip HTML and PHP tags from a string -->
                        <!-- 去除HTML標籤 -->
                        <td><?= strip_tags($row['address']) ?></td>
                        <!-- htmlentities: Convert all applicable characters to HTML entities -->
                        <!-- 跳脫HTML標籤 -->
                        <td><?= htmlentities($row['birthday']) ?></td>
                        <!-- <td>/*<?= $row['birthday'] ?>*/</td> -->
                        <td>
                            <!-- 輸入一個假連結，呼叫delete_it函式 -->
                            <a href="javascript: delete_it(<?= $row['sid'] ?>)">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
    <script>
        // 確認刪除訊息
        function delete_it(sid) {
            if (confirm(`確定要刪除編號為 ${sid} 的資料嗎?`)) {
                location.href = 'data_delete.php?sid=' + sid;
            }
        }
    </script>
<?php include __DIR__ . '/__html_foot.php'; ?>