<?php
// 摘要：連結資料庫

// 檔案名稱前面加了兩個底線，代表此檔案"非獨立頁面"。
// 此為個人命名習慣，非通用規則
// 這裡可使用'常數'或是'一般變數'
$db_host = 'localhost';
$db_name = 'mytest';
$db_user = 'root';
$db_pass = '';

// 這裡使用雙引號
// mysql固定使用雙引號
$dsn = "mysql:host={$db_host};dbname={$db_name}";

try {
    // 連結資料庫
    // 建立一個PDO的class
    $pdo = new PDO($dsn,$db_user, $db_pass);

    // 修改成utf8編碼以正常顯示中文字
    // 資料進去跟出來都使用utf8編碼
    // 先建立pdo物件，才能夠使用'query'
    $pdo->query("SET NAMES utf8");

    // 除錯??
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $ex) {
    echo 'Error: '. $ex -> getMessage();
}

if(! isset($_SESSION)){
    session_start();
}