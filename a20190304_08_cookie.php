<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<!-- 摘要：只讀取cookie -->

<?php
    // 這裡只有讀取cookie，並沒有設定，所以會直接讀取現存的cookie
    // cookie可以在不同頁面讀取，可以跨頁面
    // 只有用戶端可以清除cookie
    echo $_COOKIE['mycookie'];

?>

</body>
</html>