<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    // 摘要：php的時間函式

    // 設定預設的時區
    // List of Supported Timezones:
    // http://php.net/manual/en/timezones.php
    date_default_timezone_set('Asia/Taipei');

    echo date("Y-m-d H:i:s");
    echo '<br>';
    echo date("Y-m-d H:i:s", time() + 30*24*60*60);
    echo '<br>';

?>

</body>
</html>