<?php
// 摘要：利用PhpSpreadsheet套件將資料輸出至excel表格

// 將需要的檔案包含進來
// 提示：路徑名稱用倒斜線'\'
require 'vendor/autoload.php';

// 使用spreadsheet及xlsx類別
// 'use'同JavaScript裡的'import'
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'Hello World !')
      ->setCellValue('B1', '哈囉 !');

$writer = new Xlsx($spreadsheet);
$writer->save('hello_world.xlsx');

// 程式未完成！ 有error訊息！