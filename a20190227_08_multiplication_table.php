<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table border="1">
    <?php for($j=1; $j<=9; $j++): ?>
    <tr>
        <?php $j ?>
        <?php for($i=1; $i<=9; $i++): ?>
        <td><?= $i*$j ?></td>
        <!-- for test use only below -->
        <!-- <td><?php printf('%s * %s = %s', $i, $j, $i*$j); ?><td> -->
        
        <?php endfor ?>
    </tr>
    <?php endfor ?>
</table>


</body>
</html>