<?php

// 摘要：php檔案上傳

// 上傳檔案值路徑
$upload_dir = __DIR__. '/uploads/';



// 'my_file': 欄位名稱
// 'name': 原始檔名
$upload_file = $upload_dir. $_FILES['my_file']['name'];

// move_uploaded_file
// php會將傳完的檔案放在一個暫存的資料夾
// 'tmp_name': 暫存的檔名，沒有副檔名
// CAUTION! 若在瀏覽器出現Permission Denied訊息，請檢查目標資料匣的存取權限
// 將所有帳戶的存取權限都打開(READ & WRITE)
if(move_uploaded_file($_FILES['my_file']['tmp_name'], $upload_file)){
    // 檔案名稱
    echo "{$_FILES['my_file']['name']}<br>";
    // 檔案類型
    echo "{$_FILES['my_file']['type']}<br>";
    // 檔案大小(byte)
    echo "{$_FILES['my_file']['size']}<br>";
    // 顯示上傳成功訊息
    echo 'success';
} else {
    echo 'failed';
}