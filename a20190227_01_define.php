<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div>
    <?php
        // php的常數定義
        define('MY_CONSTANT', 3.14);
        $num = 21;
        $str = 'hello';

        echo 'MY_CONSTANT';
        echo MY_CONSTANT;
        echo '<br>';
        echo 'PHP_VERSION';
        echo PHP_VERSION;
        echo '<br>';
        echo 123;
        echo '<br>';
        echo 'echo num';
        echo '<br>';
        echo $num;
        echo '<br>';
        echo 'echo str';
        echo '<br>';
        echo $str;
        echo '<br>';
    ?>

    __DIR__: <?= __DIR__ ?><br>
    __FILE__: <?= __FILE__ ?><br>
    __LINE__: <?= __LINE__ ?><br>
    __LINE__: <?= __LINE__ ?><br>
    __LINE__: <?= __LINE__ ?><br>
    __LINE__: <?= __LINE__ ?><br>
    __LINE__: <?= __LINE__ ?><br>
</div>


</body>
</html>


