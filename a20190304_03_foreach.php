<?php

    // 關聯式陣列
    // Associated Array with modern expression
    $ar3 = array(
        'name' => 'Bill',
        'age' => '20',
        'gender' => 'male'
    );

    // Add value 100 to array, and it will assign index 0 to it automatically
    // Because in ar3, there doesn't have any indexed array element
    $ar3[] = 100; //array push

    // forech with indexed value
    // $k as key, $v as value in array
    // Remember FAT ARROW SIGN in indexed array!
    foreach ($ar3 as $k => $v) {
        echo "$k, $v <br>";
    }

    echo "--------------<br>";

    // 由網址列獲得值
    foreach ($_GET as $k => $v) {
        echo "$k, $v <br>";
    }
?>