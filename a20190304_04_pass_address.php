<?php
    // TOPIC: 以"&"符號傳遞陣列（或其他資料類型）的記憶體位址
    // 有別於JavaScript，在php裡，傳遞位址跟資料型態沒有關係
    // 類似C、C++的作法

    $ar3 = array(
        'name' => 'Bill',
        'age' => '20',
        'gender' => 'male'
    );

    // 傳值：傳遞數值
    $ar4 = $ar3;

    // 傳址：傳遞（記憶體）位址
    $ar5 = &$ar4; // 以位址設定

    $ar4['name'] = 'David';

    // print_r — Prints human-readable information about a variable
    print_r($ar3);
    print_r($ar4);
    print_r($ar5);

    $a = 10;
    $b = &$a;
    $b = 100;

    echo $a;

    // 接收變數 $a 與 $b 的位址
    function func(&$a, &$b){

    }

    function func2($a, $b){

    }

    // 傳址呼叫 5.4 之後功能取消
    // func2(&$a, &$b);