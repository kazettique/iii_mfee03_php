<?php
// The "i" after the pattern delimiter indicates a case-insensitive search
// 'i': 不區分大小寫
if (preg_match("/php/i", "PHP is the web scripting language of choice.")) {
    echo "A match was found.";
} else {
    echo "A match was not found.";
}

// preg_match: Perform a regular expression match
// REF: http://php.net/manual/en/function.preg-match.php

?>