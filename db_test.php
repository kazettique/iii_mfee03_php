<?php

// Input database from outside
// 在這裏，include 相當於將__connect_db.php檔案裡面的原始碼拷貝一份
// 因此可以使用pdo這個變數
include __DIR__ . '/__connect_db.php';

// 修改成utf8編碼以顯示中文字
//$pdo -> query("SET NAMES utf8");

try {
    // $stmt stands for 'statement'
    // '*' mark means select ALL
    // 這裡習慣是使用雙引號，因為sql裡面會使用到單引號
    // sql關鍵字不分大小寫，但是以大寫為佳，方便辨識（sql語法）
    $stmt = $pdo -> query("SELECT * FROM `address_book`");
} catch(PDOException $ex) {
    echo $ex->getMessage();
}

// Fetch with associated array.
// 抓取資料並構成關聯式陣列(FETCH_ASSOC)
//$row = $stmt -> fetch(PDO::FETCH_ASSOC);

// Fetch with indexed array.
// 抓取資料並構成索引式陣列
//$row = $stmt -> fetch(PDO::FETCH_NUM);
// Fetch with indexed & associated array.
//$row = $stmt -> fetch(PDO::FETCH_BOTH);
while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
    // print_r: Prints human-readable information about a variable
    print_r($row);
    echo "<br>";
};
