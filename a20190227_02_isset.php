<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div>
    <?php
        // 判斷是否有變數宣告
        echo isset($a) ? 'yes' : 'no';
        echo '<br>';

        $a = 123;

        echo isset($a) ? 'yes' : 'no';
        echo '<br>';

        // 字串串接
        $str1 = 'hello';
        $str2 = 'world';
        echo $str1 . $str2;


    ?>
</div>


</body>
</html>


