<?php
    // php的陣列
    // 索引式陣列（一般表達式）
    // Indexed Array with traditional expression
    $ar1 = array(1, 2, 3, 4, 5);

    // 索引式陣列（新語法）
    // Indexed Array with modern expression
    $ar2 = [5, 4, 3, 2, 1];

    // 關聯式陣列（一般表達式）
    // Associated Array with traditional expression
    // 類似JavaScript的Object：一個key對應一個value的結構
    // 使用"胖箭頭" =>
    $ar3 = array(
        'name' => 'Bill',
        'age' => '20',
        'gender' => 'male', // 最後一行可以保留逗號
    );

    // 關聯式陣列(新式表達式）
    // Associated Array with modern expression
    $ar4 = array(
        'name' => 'Bill',
        'age' => '20',
        'gender' => 'male'
    );

    // pre標籤會呈現原始的狀態（類似json的排版）
    echo '<pre>';
    // print_r：通常是除錯的時候使用
    // print_r — Prints human-readable information about a variable
    print_r($ar1);
    print_r($ar2);
    print_r($ar3);
    echo '<pre>';

    // 顯示比較多資料的訊息
    // var_dump — Dumps information about a variable
    var_dump($ar1);
