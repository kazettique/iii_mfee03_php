<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div>
    <?php
        $name = "Moto";

        echo "\"Hello, $name\"<br>";
        // Use double quote mark, then we can put variables in it
        // 使用雙引號，裡面在能放變數
        echo "Hello, $name<br>";
        // Single quote mark only deals with string
        echo 'Hello, '. $name. '<br>';
        // Use curly braces with variables
        echo "Hello, {$name}<br>";
        //
        echo "Hello, ${name}<br>";
        //
        echo "Hello, ${name}123<br>";

    ?>
</div>


</body>
</html>


