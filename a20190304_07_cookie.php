<?php
    // 摘要：設定及讀取cookie

    // 設定cookie，時間期限的單位為'秒'
    // time()為時間戳記函式
    // 瀏覽器將cookie傳至客戶端
    setcookie('mycookie', 'test value', time()+30);

?>

<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    // 讀取cookie
    // 預設的陣列變數
    echo $_COOKIE['mycookie'];

?>

</body>
</html>