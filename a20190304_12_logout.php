<?php
    // 摘要：登出頁面

    // 啓動SESSION
    session_start();

    // 必須要先啟動才能執行清除

    // 清除SESSION['user']變數
    unset($_SESSION['user']);

    // 導回登入頁面
    header('Location: a20190304_11_login.php');
