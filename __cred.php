<?php
// 摘要：

// 判斷session是否啟動
if (! isset($_SESSION)) {
    session_start();
}

//
if (! isset($_SESSION['admin'])) {
    header('Location: login.php');
    exit;
}